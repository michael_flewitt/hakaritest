package com.mflewitt;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HikariCPTest {

    public static void main(String[] args) throws InterruptedException {

        String configFile = "src/main/resources/db.properties";

        HikariConfig cfg = new HikariConfig(configFile);
        HikariDataSource ds = new HikariDataSource(cfg);

        Logger lgr = Logger.getLogger(HikariCPTest.class.getName());

        try {

            while (true)
            {
                PreparedStatement pst = null;
                ResultSet rs = null;

                Connection con = ds.getConnection();

                Thread.sleep(10000);

                // create
                lgr.log(Level.SEVERE, "create");
                pst = con.prepareStatement("CREATE TABLE mflewittTest (id INT PRIMARY KEY IDENTITY (1, 1), a VARCHAR (50) NOT NULL,b INT NOT NULL);");
                pst.execute();

                Thread.sleep(1000);

                // insert
                lgr.log(Level.SEVERE, "insert");
                pst = con.prepareStatement("INSERT INTO mflewittTest (a,b) VALUES ('1', 1);");
                pst.execute();

                Thread.sleep(1000);

                // insert
                lgr.log(Level.SEVERE, "select");
                pst = con.prepareStatement("SELECT * FROM mflewittTest");
                pst.executeQuery();

                Thread.sleep(1000);

                // delete
                lgr.log(Level.SEVERE, "delete");
                pst = con.prepareStatement("DROP TABLE mflewittTest;");
                pst.execute();

//                lgr.log(Level.SEVERE, rs.toString());
            }

        } catch (SQLException ex) {
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        }
    }
}
