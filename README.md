# HakariTest


#To Build
mvn package

#To Run required DB

docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -e 'MSSQL_PID=Express' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-latest-ubuntu 

#To Run Jar
java -javaagent:{path}/fusionreactor.jar=name=hikari,address=8099 -jar target/HikariCPTest-1.0-SNAPSHOT.jar
